package com.pepriandika.todolistapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.ScrollView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // obj edit text
        val textBox: EditText = findViewById(R.id.editTextTextMultiLine)

        // obj tombol
        val addTextButton: Button = findViewById(R.id.button)

        //obj ScrollView
        val scrollView: ScrollView = findViewById(R.id.scrollView)

        // mendapatkan text yang ditulis di textbox
        val getText = textBox.text

        // kejadian ketika tombol di AddTask disentuh
        addTextButton.setOnClickListener {
            Toast.makeText(this, "$getText", Toast.LENGTH_SHORT).show()
            getText.clear() // membersihkan text di textbox
        }

        textBox.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Gulingkan ke bagian bawah setelah teks berubah
                scrollView.fullScroll(ScrollView.FOCUS_DOWN)}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {}
        })
    }

    // action Bar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    // kejadian ketika menu di klik
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.action_show_todo_list -> {
                Toast.makeText(this, "menuju Detail Activity", Toast.LENGTH_SHORT).show()
                // Membuat Intent untuk memulai Aktivitas Baru
                val intent = Intent(this, DetailActivity::class.java)
                // Memulai Aktivitas Baru
                startActivity(intent)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}